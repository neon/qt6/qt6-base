Source: qt6-base
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Patrick Franz <deltaone@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               default-libmysqlclient-dev,
               dh-exec,
               firebird-dev [linux-any],
               glslang-tools,
               glslc,
               libatspi2.0-dev,
               libb2-dev,
               libbrotli-dev,
               libcups2-dev,
               libdbus-1-dev,
               libdouble-conversion-dev,
               libdrm-dev [linux-any],
               libegl-dev,
               libfontconfig-dev,
               libfreetype-dev,
               libgbm-dev [linux-any],
               libgif-dev,
               libgl-dev,
               libgles-dev,
               libglib2.0-dev,
               libglu1-mesa-dev | libglu-dev,
               libglx-dev,
               libgtk-3-dev,
               libharfbuzz-dev,
               libice-dev,
               libicu-dev,
               libinput-dev [linux-any],
               libjpeg-dev,
               libkrb5-dev,
               liblttng-ust-dev [linux-any],
               libmd4c-dev,
               libmd4c-html0-dev,
               libmtdev-dev [linux-any],
               libopengl-dev,
               libpcre2-dev,
               libpng-dev,
               libpq-dev,
               libproxy-dev,
               libsctp-dev [linux-any],
               libsqlite3-dev,
               libssl-dev,
               libsystemd-dev [linux-any],
               libts-dev,
               liblttng-ust-dev,
               libturbojpeg0-dev,
               libudev-dev [linux-any],
               libvulkan-dev [linux-any],
               libwayland-dev [linux-any],
               libx11-dev,
               libx11-xcb-dev,
               libxcb1-dev,
               libxcb-cursor-dev,
               libxcb-composite0-dev,
               libxcb-damage0-dev,
               libxcb-dpms0-dev,
               libxcb-dri2-0-dev,
               libxcb-dri3-dev,
               libxcb-ewmh-dev,
               libxcb-glx0-dev,
               libxcb-icccm4-dev,
               libxcb-image0-dev,
               libxcb-keysyms1-dev,
               libxcb-present-dev,
               libxcb-randr0-dev,
               libxcb-record0-dev,
               libxcb-render-util0-dev,
               libxcb-render0-dev,
               libxcb-res0-dev,
               libxcb-screensaver0-dev,
               libxcb-shape0-dev,
               libxcb-shm0-dev,
               libxcb-sync-dev,
               libxcb-util-dev,
               libxcb-xfixes0-dev,
               libxcb-xf86dri0-dev,
               libxcb-xinerama0-dev,
               libxcb-xinput-dev,
               libxcb-xkb-dev,
               libxcb-xrm-dev,
               libxcb-xv0-dev,
               libxcb-xvmc0-dev,
               libxcb-xtest0-dev,
               libxext-dev,
               libxfixes-dev,
               libxi-dev,
               libxkbcommon-dev,
               libxkbcommon-x11-dev,
               libxrender-dev,
               libzstd-dev,
               mold,
               ninja-build,
               pkgconf,
               pkg-kde-tools-neon,
               shared-mime-info,
               unixodbc-dev,
               zlib1g-dev,
Build-Depends-Indep: qt6-base-dev (>=6.8~) <!nodoc>,
                     qt6-documentation-tools (>=6.8~) <!nodoc>,
                     qt6-base-doc (>=6.8~) <!nodoc>,
Standards-Version: 4.6.2
Homepage: https://www.qt.io/developers/
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/qt-kde-team/qt6/qt6-base.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/qt6/qt6-base

Package: qt6-base
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends},
Depends: fontconfig,
         shared-mime-info,
         ${misc:Depends},
         ${shlibs:Depends}
Provides: qt6-base-private-abi (=6.8.2),
Recommends: libcups2,
            libqt6sql6-sqlite | libqt6sql6-mysql | libqt6sql6-odbc | libqt6sql6-psql | libqt6sql6-ibase,
            qt6-gtk-platformtheme,
            qt6-qpa-plugins,
            qt6-wayland,
            qt6-translations-l10n
Breaks: qt6-qpa-plugins (<< 6.3.1+dfsg-6~),
        libqt6gui6 (<< 6.8.1),
        libqt6gui6t64 (<< 6.8.1),
        libqt6network6 (<< 6.2.4+dfsg-5~),
        libqt6opengl6 (<< 6.3.1+dfsg-6~),
        libqt6opengl6t64 (<< 6.4.2+dfsg-21~),
Replaces: libqt6gui6t64 (<< 6.8.1),
          libqt6network6 (<< 6.2.4+dfsg-5~),
          libqt6opengl6t64 (<< 6.4.2+dfsg-21~),
          qt6-qpa-plugins (<< 6.3.1+dfsg-6~),
Description: Qt 6 core module
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 The QtCore module contains core non-GUI functionality.

Package: qt6-base-dev
Architecture: any
Section: kde
X-Neon-MergedPackage: true
Pre-Depends: ${misc:Pre-Depends},
Depends: default-libmysqlclient-dev,
         firebird-dev,
         glslang-tools,
         glslc,
         libatspi2.0-dev,
         libb2-dev,
         libbrotli-dev,
         libcups2-dev,
         libdbus-1-dev,
         libdouble-conversion-dev,
         libdrm-dev,
         libegl-dev,
         libfontconfig-dev,
         libfreetype-dev,
         libgif-dev,
         libgbm-dev,
         libgl-dev,
         libgles-dev,
         libglib2.0-dev,
         libglu1-mesa-dev | libglu-dev,
         libglx-dev,
         libgtk-3-dev,
         libharfbuzz-dev,
         libice-dev,
         libicu-dev,
         libinput-dev,
         libjpeg-dev,
         libkrb5-dev,
         liblttng-ust-dev,
         libmd4c-dev,
         libmd4c-html0-dev,
         libmtdev-dev,
         libopengl-dev,
         libpcre2-dev,
         libpng-dev,
         libpq-dev,
         libproxy-dev,
         libsctp-dev,
         libsystemd-dev,
         libsqlite3-dev,
         libssl-dev,
         libts-dev,
         liblttng-ust-dev,
         libturbojpeg0-dev,
         libudev-dev,
         libvulkan-dev,
         libwayland-dev,
         libx11-dev,
         libx11-xcb-dev,
         libxcb1-dev,
         libxcb-cursor-dev,
         libxcb-composite0-dev,
         libxcb-damage0-dev,
         libxcb-dpms0-dev,
         libxcb-dri2-0-dev,
         libxcb-dri3-dev,
         libxcb-ewmh-dev,
         libxcb-glx0-dev,
         libxcb-icccm4-dev,
         libxcb-image0-dev,
         libxcb-keysyms1-dev,
         libxcb-present-dev,
         libxcb-randr0-dev,
         libxcb-record0-dev,
         libxcb-render-util0-dev,
         libxcb-render0-dev,
         libxcb-res0-dev,
         libxcb-screensaver0-dev,
         libxcb-shape0-dev,
         libxcb-shm0-dev,
         libxcb-sync-dev,
         libxcb-util-dev,
         libxcb-xfixes0-dev,
         libxcb-xf86dri0-dev,
         libxcb-xinerama0-dev,
         libxcb-xinput-dev,
         libxcb-xkb-dev,
         libxcb-xrm-dev,
         libxcb-xv0-dev,
         libxcb-xvmc0-dev,
         libxcb-xtest0-dev,
         libxext-dev,
         libxfixes-dev,
         libxi-dev,
         libxkbcommon-dev,
         libxkbcommon-x11-dev,
         libxrender-dev,
         libzstd-dev,
         python3:any,
         qt6-base (= ${binary:Version}),
         unixodbc-dev,
         zlib1g-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Breaks: libqt6opengl6-dev (<< 6.4.2+dfsg-12~),
        qt6-base (<< 6.6.2-1neon),
        qt6-base-doc (<< ${source:Version}~ciBuild),
Replaces: libqt6opengl6-dev (<< 6.4.2+dfsg-12~),
Description: Qt 6 base development files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the header development files used for building
 Qt 6 applications.
 .
 If you are interested in packaging Qt 6 stuff please take a look at
 https://qt-kde-team.pages.debian.net/packagingqtbasedstuff.html

Package: qt6-base-doc
Build-Profiles: <!nodoc>
Architecture: all
Section: kde
X-Neon-MergedPackage: true
Depends: qt6-base-dev, ${misc:Depends}
Replaces: qt6-base-doc-html,
Description: Qt 6 base documentation files
 Qt is a cross-platform C++ application framework. Qt's primary feature
 is its rich set of widgets that provide standard GUI functionality.
 .
 This package contains the documentation files used to help build
 Qt 6 applications.
 .
 If you are interested in packaging Qt 6 stuff please take a look at
 https://qt-kde-team.pages.debian.net/packagingqtbasedstuff.html

#### transitionals below

Package: libqt6core6
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6core6t64
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6concurrent6
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6dbus6
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6dbus6t64
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6gui6
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6gui6t64
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6network6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6network6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6opengl6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6opengl6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6opengl6-dev
Architecture: all
Depends: qt6-base-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6openglwidgets6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6openglwidgets6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6printsupport6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6printsupport6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6concurrent6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6-mysql
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6-odbc
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6-psql
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6-sqlite
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6sql6-ibase
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6test6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6test6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6widgets6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6widgets6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6xml6
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: libqt6xml6t64
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qmake6
Architecture: all
Depends: qt6-base-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qmake6-bin
Architecture: all
Depends: qt6-base-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-base-doc-dev
Architecture: all
Depends: qt6-base-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-base-doc-html
Architecture: all
Depends: qt6-base-doc, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-base-dev-tools
Architecture: all
Depends: qt6-base-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-base-private-dev
Architecture: all
Depends: qt6-base-dev, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-gtk-platformtheme
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-qpa-plugins
Architecture: all
Depends: qt6-base,  ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.

Package: qt6-xdgdesktopportal-platformtheme
Architecture: all
Depends: qt6-base, ${misc:Depends}
Description: Dummy transitional
 Transitional dummy package.